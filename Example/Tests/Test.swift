//
//  File.swift
//  KernColors_Example
//
//  Created by Cristian Azov on 31.08.20.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import XCTest
import KernColors
import SwiftUI

class Test: XCTestCase {

  func testColor() throws {
    XCTAssertNotNil(UIColor.textColorExample)
    XCTAssertNotNil(Color.textColorExample)
    XCTAssertNotNil(UIColor.backgroundColorExample)
    XCTAssertNotNil(Color.backgroundColorExample)
  }
}
