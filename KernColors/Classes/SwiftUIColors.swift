// Generated file - avoid manual edit!

import SwiftUI

public extension Color {
	static let textColorExample = Color.color(UIColor(red: 0, green: 0, blue: 0, alpha: 1), dark: UIColor(red: 1, green: 1, blue: 1, alpha: 1))
	static let backgroundColorExample = Color.color(UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1), dark: UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 1))
}

extension Color {
  /// If the dark color is not provided, then in dark mode the light one is returned instead.
  static func color(_ light: UIColor, dark: UIColor?) -> Color {
    Color(UIColor { traitCollection -> UIColor in
      switch traitCollection.userInterfaceStyle {
      case .light, .unspecified: return light
      case .dark: return dark ?? light
      @unknown default:
        assertionFailure("Uncovered UIUserInterfaceStyle=\(traitCollection.userInterfaceStyle). Returning .light")
        return light
      }
    })
  }
}
