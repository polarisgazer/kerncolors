#!/usr/bin/env xcrun --sdk macosx swift

//  Created by Cristian Azov on 30.08.20.
//  Copyright © 2019 Cristian Azov. All rights reserved.
//

import Foundation

// MARK: - String Extensions
extension String {

  func appending(pathComponent: String) -> String {
    return URL(fileURLWithPath: self).appendingPathComponent(pathComponent).path
  }
}

// MARK: - ColorSet Struct
struct ColorSet: Decodable {
  enum CodingKeys: String, CodingKey {
    case colors
  }

  var name = ""

  var colors: [Color]

  // swiftlint:disable nesting
  struct Color: Decodable {
    struct Appearance: Decodable {
      enum Value: String, Decodable {
        case dark
        case light
      }

      var appearance: String
      var value: Value
    }

    struct Color: Decodable {
      var components: Components?
      var reference: String?

      struct Components: Decodable {
        var red: String?
        var green: String?
        var blue: String?
        var white: String?
        var alpha: String?
      }
    }
    // swiftlint:enable nesting

    var appearances: [Appearance]?

    var color: Color
  }
}

extension ColorSet.Color {
  var appearance: ColorSet.Color.Appearance.Value {
    guard let appearances = self.appearances, appearances.count > 0 else { return .light }
    return appearances.contains(where: { $0.value == .dark }) ? .dark : .light
  }
}

extension ColorSet {

  var lightColor: String? {
    guard let components = self.colors.first(where: { $0.appearance == .light})?.color.components else { return nil }
    return colorString(from: components, forAsset: self.name)
  }

  var darkColor: String? {
    guard let components = self.colors.first(where: { $0.appearance == .dark})?.color.components else { return nil }
    return colorString(from: components, forAsset: self.name)
  }
}

let assetPath = "KernColors/Assets/Colors.xcassets"
let uiColorsOutputPath =  "KernColors/Classes/UIColors.swift"
let swiftUIColorsOutputPath =  "KernColors/Classes/SwiftUIColors.swift"

// MARK: - Color File Content

var uiColorsFile = """
//
// Generated file. Please do not edit, nor delete.
//

import UIKit

public extension UIColor {
%@
}

extension UIColor {
  /// If the dark color is not provided, then in dark mode the light one is returned instead.
  static func color(_ light: UIColor, dark: UIColor?) -> UIColor {
    UIColor { traitCollection -> UIColor in
      switch traitCollection.userInterfaceStyle {
      case .light, .unspecified: return light
      case .dark: return dark ?? light
      @unknown default:
        assertionFailure(\"Uncovered UIUserInterfaceStyle=\\(traitCollection.userInterfaceStyle). Returning .light\")
        return light
      }
    }
  }
}
"""

var swiftUIColorsFile = """
// Generated file - avoid manual edit!

import SwiftUI

public extension Color {
%@
}

extension Color {
  /// If the dark color is not provided, then in dark mode the light one is returned instead.
  static func color(_ light: UIColor, dark: UIColor?) -> Color {
    Color(UIColor { traitCollection -> UIColor in
      switch traitCollection.userInterfaceStyle {
      case .light, .unspecified: return light
      case .dark: return dark ?? light
      @unknown default:
        assertionFailure(\"Uncovered UIUserInterfaceStyle=\\(traitCollection.userInterfaceStyle). Returning .light\")
        return light
      }
    })
  }
}
"""

// MARK: - FileManager Handling
func fetchAssets(at path: String) -> [String] {
  let directorEnumerator = FileManager.default.enumerator(atPath: path)

  var paths = [String]()

  while let file = directorEnumerator?.nextObject() as? String {
    if file.hasSuffix(".colorset") {
      paths.append(path.appending(pathComponent: file))
    }
  }

  return paths
}

func colorString(from components: ColorSet.Color.Color.Components, forAsset asset: String) -> String? {
  let alpha = components.alpha?.truncatingZeroes ?? "1"

  if
    let red = components.red?.truncatingZeroes,
    let green = components.green?.truncatingZeroes,
    let blue = components.blue?.truncatingZeroes
  {
    return "UIColor(red: \(red), green: \(green), blue: \(blue), alpha: \(alpha))"
  } else if let white = components.white?.truncatingZeroes {
    return "UIColor(white: \(white), alpha: \(alpha))"
  }
  return nil
}

func colorStrings(assets: [String], isForSwiftUI: Bool) -> [String] {
  var colors: [String] = []
  for asset in assets {
    guard
        let data = try? Data(contentsOf: URL(fileURLWithPath: asset.appending(pathComponent: "Contents.json"))),
        var colorSet = try? JSONDecoder().decode(ColorSet.self, from: data) else { continue }

    let name = URL(fileURLWithPath: asset).deletingPathExtension().lastPathComponent
    colorSet.name = name
    guard let lightColor = colorSet.lightColor else { continue }
    colors.append("\tstatic let \(name) = \(isForSwiftUI ? "" : "UI")Color.color(\(lightColor), dark: \(colorSet.darkColor ?? "nil"))"
    )
  }
  return colors
}

func write(lines: [String], in fileFormat: String, at outputPath: String) {
  if lines.isEmpty {
    try? "".write(toFile: outputPath, atomically: true, encoding: .utf8)
  } else {
    let fileContent = String(format: fileFormat, lines.joined(separator: "\n"))
    try? fileContent.write(toFile: outputPath, atomically: true, encoding: .utf8)
  }
}

let assets = fetchAssets(at: assetPath)

let uiColors = colorStrings(assets: assets, isForSwiftUI: false)
let swiftUIColors = colorStrings(assets: assets, isForSwiftUI: true)
write(lines: uiColors, in: uiColorsFile, at: uiColorsOutputPath)
write(lines: swiftUIColors, in: swiftUIColorsFile, at: swiftUIColorsOutputPath)

func shell(_ command: String) -> String {
    let task = Process()
    let pipe = Pipe()

    task.standardOutput = pipe
    task.arguments = ["-c", command]
    task.launchPath = "/bin/bash"
    task.launch()

    let data = pipe.fileHandleForReading.readDataToEndOfFile()
    let output = String(data: data, encoding: .utf8)!

    return output
}

print(shell("swiftlint --fix --quiet"))
print(shell("echo \"Color conversion completed.\""))

extension String {
    var truncatingZeroes: String {
        guard let doubleValue = Double(self) else { return self }
        return String(format: "%g", doubleValue)
    }
}
