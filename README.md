# KernColors

This is a template pod for creating "Colors" pods: it allows to add colors in .xcassets, and generates 
static instances in the UIColor (and Color for SwiftUI) extension.

[![CI Status](https://img.shields.io/travis/polarisgazer/KernColors.svg?style=flat)](https://travis-ci.org/polarisgazer/KernColors)
[![Version](https://img.shields.io/cocoapods/v/KernColors.svg?style=flat)](https://cocoapods.org/pods/KernColors)
[![License](https://img.shields.io/cocoapods/l/KernColors.svg?style=flat)](https://cocoapods.org/pods/KernColors)
[![Platform](https://img.shields.io/cocoapods/p/KernColors.svg?style=flat)](https://cocoapods.org/pods/KernColors)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KernColors is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following lines to your Podfile:

```ruby
source 'git@bitbucket.org:polarisgazer/kern-specs.git'

pod 'KernColors'
```

## Adding a color

- Add a new color set in the `Colors` folder whithin  `Colors.xcassets` from the `Resources` folder
- Rename the newly created color set (this name will be generated in the UIColor extension)
- Select the color in the editor pane and the Attributes Inspector tab
- Check `Universal` in the `Devices` list
- Select `Any, Dark` as `Appearances`
- For every appearance select `Extended range sRGB` as `Content`
- After editing the color RGB values, regardless of the `Input method`, select `Floating point (0.0-1.0)` (this format is supported by the conversion script)
- Bild the project: a run script phase will make the conversion
- You can manually covert by running `./ColorParser.swift` or directly `swift ColorParser.swift`

## Contributing
- After editing `ColorParser.swift`, compile it with `swiftc ColorParser.swift` in order the changes to take effect
- Please resolve all Swiftlint complaints

## Author

Cristian Azov, polarisgazer@gmail.com

## License

KernColors is available under the MIT license. See the LICENSE file for more info.
